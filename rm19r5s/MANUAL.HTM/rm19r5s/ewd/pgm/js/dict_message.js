/*! All Rights Reserved. Copyright 2012 (C) TOYOTA  MOTOR  CORPORATION.
Last Update: 2012/10/22 */

/**
 * file dict.js<br />
 *
 * @fileoverview 多言語対応辞書クラス。（仏語）<br />
 * file-> dict_message.js
 * @author Imamura
 * @version 1.0.0
 *
 * History(date|version|name|desc)<br />
 *  2012/10/22|1.0.0|Shimizu|新規作成<br />
 */
/*-----------------------------------------------------------------------------
 * サービス情報高度化プロジェクト
 * 更新履歴
 * 2012/10/22 Imamura ・新規作成
 *---------------------------------------------------------------------------*/
/**
 * 多言語対応辞書クラス（仏語）
 * @namespace 多言語対応辞書クラス（仏語）
 */
var Dict = {
    //key : value,
    MVWF1001AAE : '{0} es un elemento requerido.',
    MVWF1002AAE : 'Para {0}, \nasegúrese de introducir al menos {1} o más caracteres, \ny no superar los {2} caracteres.',
    MVWF1003AAE : 'Para {0}, introduzca exactamente {1} carácter.',
    MVWF1004AAE : 'Para {0}, introduzca {1} caracteres.',
    MVWF1005AAE : 'Para {0}, \nintroduzca caracteres alfabéticos en mayúscula de 1 byte.',
    MVWF1006AAE : 'Para {0}, \nintroduzca caracteres alfabéticos en minúscula de 1 byte.',
    MVWF1007AAE : 'Para {0}, \nintroduzca caracteres numéricos de 1 byte.',
    MVWF1008AAE : '{0} contiene un carácter inválido.\nPara más información, consulte la Ayuda.',
    MVWF1009AAE : 'No pueden introducirse espacios de 1 byte en {0}.',
    MVWF1010AAE : 'Para {0}, \nintroduzca caracteres de 2 bytes.',
    MVWF1011AAE : 'No se introdujeron datos en {0}.',
    MVWF1012AAE : 'Para {0}, introduzca {1}.',
    MVWF1013AAE : 'La fecha introducida es incorrecta. ',
    MVWF1014AAE : 'Para {0}, \nintroduzca una fecha antes de {1}.',
    MVWF1015AAE : 'Para {0}, \nintroduzca una fecha después de {1}.',
    MVWF1016AAE : 'Para {0}, \nasegúrese de introducir al menos {1} o más caracteres, \ny no superar los {2} caracteres.',
    MVWF1017AAE : 'Para {0}, \nintroduzca con caracteres alfanuméricos y {1}.',
    MVWF1018AAE : 'Para {0}, \nintroduzca con {1} y {2}.',
    MVWF1019AAE : 'Para {0}, \nintroduzca caracteres alfanuméricos de 1 byte.',
    MVWF0001AAE : 'Introduzca n.º VIN/chasis',
    MVWF0002DAE : 'No existe información del vehículo que coincida con su solicitud.\nConfirme el n.º VIN/chasis introducido.\nEj. VIN: 17 dígitos alfanuméricos\n     Chasis n.º: ZVW30-1234567\nO seleccione la información del vehículo del menú desplegable.',
    MVWF0003AAE : 'La información de servicio no puede abrirse.\nporque no se seleccionó la información del vehículo.\nSeleccione los elementos requeridos (*) del menú desplegable.',
    MVWF0004AAE : 'Introduzca palabras de búsqueda.',
    MVWF0005AAE : 'Introduzca palabras de búsqueda con un máximo de 200 caracteres.',
    MVWF0006AAE : 'Introduzca la palabra YOKOGUSHI sin espacios en medio \ny con 5 o más caracteres.',
    MVWF0007AAE : 'Introduzca un máximo de 30 palabras de búsqueda.',
    MVWF0008AAE : 'No existe información actualizada.',
    MVWF0009AAE : 'La palabra contiene un carácter inválido.\nConsulte {0} e introduzca de nuevo las palabras.',
    MVWF0010DAE : 'No es posible recuperar {0}.',
    MVWF0011AAE : 'Falló el inicio de Techstream.\nConfirme que se cumplen las siguientes condiciones:\n- Techstream está instalado.\n- Techstream puede iniciarse independientemente.\nCódigo de error: ',
    MVWF0012AAE : 'La operación falló porque Techstream estaba procesando.\nReintentar tras confirmar la información de \nTechstream que aparece en pantalla.\nCódigo de error: ',
    MVWF0013AAE : 'La operación falló porque la siguiente función estaba ejecutándose.\nFinalizar la siguiente función y reintentar.\n  Utilidad, CARB OBD II, CUW, SUW\nCódigo de error: ',
    MVWF0014AAE : 'La operación falló.\nSi Techstream ya está iniciado, finalizar Techstream y reintentar.\nCódigo de error: ',
    MVWF0015AAE : 'La operación falló.\nCódigo de error:',
    MVWF0016AAE : 'La operación falló.\nConfirme que se cumple la siguiente condición:\n- Techstream está instalado.\nCódigo de error: ',
    MVWF0017AAE : 'La operación falló porque Techstream está ejecutándose \nen otro navegador.\nCerrar el otro navegador y reintentar.\nCódigo de error: ',
    MVWF0021AAI : 'Preparando...',
    MVWF0022AAI : 'Preparación: OK',
    MVWF0023AAI : 'Iniciando Techstream...',
    MVWF0024AAI : 'Inicio Techstream: OK',
    MVWF0025AAI : 'Iniciando función...',
    MVWF0026AAI : '',
    MVWF0027AAI : '',
    MVWF0028AAE : '0 resultados de búsqueda',
    MVWF0029AAE : 'El título seleccionado no existe.\nCódigo de piezas: {0}',
    MVWF0031AAE : 'Seleccione un título.',
    MVWF0032AAE : 'Los resultados de búsqueda superan el número máximo de registros, 500.\nAñadir opciones de búsqueda para restringir la búsqueda y reintentar.',
    MVWF0033AAE : 'Introduzca una descripción con un máximo de {0} caracteres.',
    MVWF0034AAE : 'Seleccione opciones de búsqueda.',
    MVWF0035AAE : 'Seleccione una palabra.',
    MVWF0036DEE : 'Ocurrió un error de búsqueda.',
    MVWF0037DAI : 'La búsqueda falló porque no existen manuales que \ncoincidan con su solicitud.',
    MVWF0038AAI : 'No es posible porque no existen manuales que \ncoincidan con su solicitud.',
    MVWF0041AAI : 'No existe feedback identificado.',
    MVWF0042AAE : 'Seleccione un elemento de feedback.',
    MVWF0043AAE : 'No es posible debido a que el feedback seleccionado fue aprobado.',
    MVWF0044AAC : '¿Desea el feedback seleccionado?',
    MVWF0045AAE : 'No puede responder porque el feedback seleccionado \nno fue aprobado o ya ha sido aceptado.',
    MVWF0046AAC : '¿Desea eliminar la respuesta?',
    MVWF0047AAE : 'No se seleccionó un feedback.',
    MVWF0048AAE : '',
    MVWF0049AAE : 'El feedback seleccionado ha sido actualizado/eliminado.\nSeleccione de nuevo el feedback de la Lista de feedback.',
    MVWF0050AAC : '¿Desea eliminar el archivo adjunto?\nEl archivo adjunto se eliminará cuando pulse el botón "Registrar".',
    MVWF0051AAE : 'Introducir modelo.',
    MVWF0052AAE : 'Seleccionar modelo.',
    MVWF0053AAI : '',
    MVWF0054AAE : 'El modelo seleccionado ya fue añadido.',
    MVWF0055AAC : 'Los datos introducidos se eliminarán cuando se modifique el elemento.\n¿Desea modificar el elemento?',
    MVWF0056AAE : 'Sólo es posible adjuntar un archivo cada vez.\nSi desea adjuntar un nuevo archivo, elimine el archivo adjunto existente.',
    MVWF0057AAE : 'No puede responder porque no está autorizado.',
    MVWF0058AAE : 'Las fechas están invertidas.\nCorrija las fechas y busque de nuevo.',
    MVWF0059AAE : 'No es posible registrar la extensión {0} como archivo adjunto.',
    MVWF0060AAE : 'Sólo acepta un archivo de hasta 2 MB.',
    MVWF0061AAE : 'Los resultados de la búsqueda superan los 300.\nCorrija los criterios y busque de nuevo.',
    MVWF0062AAE : 'No puede responder porque TMC no recibió el feedback seleccionado.',
    MVWF0063AAE : 'Es posible añadir hasta 50 vehículos.',
    MVWF0071AAE : 'Introducir código de empresa.',
    MVWF0072AAC : 'El resultado de la búsqueda se eliminará cuando pulse el botón "Buscar".\n¿Desea realizar la búsqueda?',
    MVWF0073DAE : 'El código de empresa introducido no existe.',
    MVWF0074DAE : 'Los resultados de la búsqueda superan los 100.\nCorrija los criterios y busque de nuevo.',
    MVWF0075AAE : 'Seleccione un archivo CSV.',
    MVWF0076DAE : 'No es posible abrir el archivo CSV.\nCompruebe el archivo CSV.',
    MVWF0077DAE : 'Es posible actualizar hasta 100 registros cada vez.\nCompruebe el archivo CSV.',
    MVWF0078DAE : 'El formato del archivo CSV no es correcto.\nCompruebe el archivo CSV.',
    MVWF0079AAE : 'Los datos en rojo son incorrectos.\nCorrija el archivo CSV y cárguelo de nuevo.',
    MVWF0080AAE : 'No existe información actualizada.',
    MVWF0081DAE : 'El código de empresa introducido para n.º {0} ya está registrado.',
    MVWF0082DAE : 'No existe un código de empresa introducido para n.º {0}.',
    MVWF0083AAE : 'El registro falló.\nContacte con el administrador del sistema.',
    MVWF0084AAI : 'Se completó el registro.',
    MVWF0085DAE : 'No existe un administrador que se corresponda con las \ncondiciones de búsqueda. ',
    MVWF0086AAE : 'Es posible actualizar hasta 10 registros cada vez.',
    MVWF0087AAE : 'Seleccione un administrador.',
    MVWF0088AAE : 'No existen datos para registrar.',
    MVWF0089DAE : 'No existe código de empresa.',
    MVWF0090DAE : 'La ID del usuario ya ha sido registrada.',
    MVWF0091DAE : 'No existe una ID de usuario.',
    MVWF0092DAE : 'No puede actualizar porque no está autorizado.',
    MVWF0093DAE : 'No existen datos actualizables en el archivo CSV. \nCompruebe el archivo CSV.',
    MVWF0094DAE : 'El código de empresa ya ha sido registrado.\nCompruebe el archivo CSV.',
    MVWF0095DAE : 'Los datos seleccionados ya fueron actualizados.',
    MVWF0096AAE : 'El archivo supera los 50 KB.',
    MVWF0101AAE : 'Seleccione el registro de trabajo de la Lista de registros de trabajo.',
    MVWF0102AAC : '¿Desea eliminar el registro de trabajo seleccionado de la lista?',
    MVWF0111AAE : 'El registro de trabajo supera las 100 anotaciones.\nEs posible registrar hasta 100 registros de trabajo por persona.\nElimine registros de trabajo de la Lista de registros de trabajo.',
    MVWF0121DAE : 'No existen datos para mostrar.\nVuelva a confirmar la información actualizada.',
    MVWF0122DAE : 'No puede utilizar el sistema porque no está autorizado.\nContacte con el administrador del sistema.',
    MVWF0123DAE : 'Ocurrió un error inesperado.',
    MVWF0124DAE : 'Cerrar el navegador y reintentar.',
    MVWF0125DAE : 'Error del sistema',
    CONST_SYSTEM_ERROR_MESSAGE : 'Mensaje',
    MVWF0064AAE : '',
    MVWF0065AAC : '',
    MVWF0066AAC : '',
    MVWF0067AAI : '',
    MVWF0068AAI : '',
    MVWF0123AAE : '',
    CONST_CARINFO_TITLE:              'Información del vehículo',
    CONST_DIAGNOSISMEMO_TITLE:        'Reg. trabajo',
    CONST_TOP_OTHER_MANUAL_TITLE:     'Información de referencia - Toyota Service Information',
    CONST_CONTENTS_TITLE_DIAGNOSTIC:  'Ayuda de diagnóstico - Toyota Service Information',
    CONST_CONTENTS_TITLE_INSPECTION:  'Procedimiento de inspección - Toyota Service Information',
    CONST_CONTENTS_TITLE_REFERENCE:   'Página de referencia - Toyota Service Information',
    CONST_CONTENTS_FLOW_NAVE_TITLE:   'Visión general',
    CONST_CONTENTS_FLOW_BTN_PROC:     'Procedimientos',
    CONST_CONTENTS_FLOW_BTN_JUDGE:    'Criterios',
    CONST_TITLESELECT_TITLE:          'Selección de título',
    CONST_FLOW_TITLE:                 'Precaución/Aviso/Sugerencia solución problemas',
    CONST_CONTENTS_TITLE_PRINT:       'Imprimir - Toyota Service Information',
    CONST_SEARCH_OPTION_TITLE:        'Opción búsqueda',
    CONST_INDEX_NAME:                 'Índice',
    CONST_LANGARY_EN:                 'English',
    CONST_LANGARY_FR:                 'Français',
    CONST_LANGARY_DE:                 'Deutsch',
    CONST_LANGARY_JA:                 '日本語',
    CONST_LANGARY_ES:                 'Español',
    CONST_SEARCH_OPTION_RETURN_TRUE:  'Sí',
    CONST_SEARCH_OPTION_RETURN_FALSE: 'No',
    CONST_TOP_WHATSNEW_COUNT:         '{1}-{2} of {0}',
    CONST_TOP_UPDATEICN_NAME:         'Actualizar detalles',
    CONST_GTS_FACEBOX_TITLE:          'Enviando/recibiendo GTS...',
    CONST_CONTENTS_FIXED_OTHER_HEAD:  '<div lang="es" class="fontEs" id="header_head"><div id="banner"><img src="../img/png/toyota.png" width="113" height="24" alt="TOYOTA" title="TOYOTA"></div><div id="navigation"><p><a href="javascript:void(0);" class="link" id="lnk_close">Salir</a></p></div></div>',
    CONST_FACEBOX_IMAGE_TITLE:        'Salir',
    CONST_GLOBAL_INDEX:               '\
<div id="index_search_contents">\
<div id="index_search_body">\
<div id="initial">\
<ul id="shift_initial">\
<li>\
<a href="javascript:void(0);" id="kanaLnk" tabIndex="1002">Kana</a>\
</li>\
<li>\
<a href="javascript:void(0);" id="alphaLnk" tabIndex="1003">Alfanumérico</a>\
</li>\
</ul>\
<div id="alphanum_list" class="initial_list invisible">\
<div class="alphabet">\
<a href="javascript:void(0);" class="initial" tabIndex="1004">A</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">B</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">C</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">D</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">E</a>\
<br>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">F</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">G</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">H</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">I</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">J</a>\
<br>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">K</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">L</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">M</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">N</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">O</a>\
<br>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">P</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">Q</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">R</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">S</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">T</a>\
<br>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">U</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">V</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">W</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">X</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">Y</a>\
<br>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">Z</a>\
<br>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">Á</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">É</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">Í</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">Ñ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">Ó</a>\
<br>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">Ú</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">Ü</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">á</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">é</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">í</a>\
<br>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ñ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ó</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ú</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ü</a>\
</div>\
<div class="number">\
<a href="javascript:void(0);" class="initial" tabIndex="1004">1</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">2</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">3</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">4</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">5</a>\
<br>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">6</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">7</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">8</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">9</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">0</a>\
</div>\
</div>\
<div id="katakana_list" class="initial_list invisible">\
<div class="kana">\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ア</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">イ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ウ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">エ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">オ</a>\
<br>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">カ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">キ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ク</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ケ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">コ</a>\
<br>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">サ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">シ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ス</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">セ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ソ</a>\
<br>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">タ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">チ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ツ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">テ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ト</a>\
<br>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ナ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ニ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ヌ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ネ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ノ</a>\
<br>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ハ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ヒ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">フ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ヘ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ホ</a>\
<br>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">マ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ミ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ム</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">メ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">モ</a>\
<br>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ヤ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ユ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ヨ</a>\
<br>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ラ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">リ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ル</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">レ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ロ</a>\
<br>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ワ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ヲ</a>\
<a href="javascript:void(0);" class="initial" tabIndex="1004">ン</a>\
</div>\
<div class="kanji">\
<a href="javascript:void(0);" class="initial" tabIndex="1004">漢字</a>\
</div>\
</div>\
</div>\
<div id="index_area">\
<div class="list_head">\
<div class="list_title">Lista índice</div>\
<div class="list_pager">\
<ul class="list_pager">\
<li class="invisible"><a id="firstPageIcn" tabIndex="1005" href="javascript:void(0);"><img src="./img/png/page_first.png" width="32" height="22" alt="<<" title="<<"></a></li>\
<li><a id="firstPageIcn_g" tabIndex="1005" href="javascript:void(0)"><img src="./img/png/page_first_g.png" width="32" height="22" alt="<<" title="<<"></a></li>\
<li class="invisible"><a id="prevPageIcn" tabIndex="1006" href="javascript:void(0);"><img src="./img/png/page_prev.png" width="32" height="22" alt="<" title="<"></a></li>\
<li><a id="prevPageIcn_g" tabIndex="1006" href="javascript:void(0)"><img src="./img/png/page_prev_g.png" width="32" height="22" alt="<" title="<"></a></li>\
<li class="invisible"><a id="nextPageIcn" tabIndex="1007" href="javascript:void(0);"><img src="./img/png/page_next.png" width="32" height="22" alt=">" title=">"></a></li>\
<li><a id="nextPageIcn_g" tabIndex="1007" href="javascript:void(0)"><img src="./img/png/page_next_g.png" width="32" height="22" alt=">" title=">"></a></li>\
<li class="invisible"><a id="lastPageIcn" tabIndex="1008" href="javascript:void(0);"><img src="./img/png/page_last.png" width="32" height="22" alt=">>" title=">>"></a></li>\
<li><a id="lastPageIcn_g" tabIndex="1008" href="javascript:void(0)"><img src="./img/png/page_last_g.png" width="32" height="22" alt=">>" title=">>"></a></li>\
</ul>\
</div>\
<div class="list_count" id="index_list_count"></div>\
</div>\
<div id="index_list"></div>\
</div>\
</div>\
<div id="index_search_foot">\
<input class="button_style" type="button" id="okBtn" tabIndex="1010" value="OK">\
<input class="button_style" type="button" id="cancelBtn" tabIndex="1011" value="Cancelar">\
</div>\
</div>',
    CONST_CONTENTS_FIXED_HEAD: '\
<div id="header_head">\
<div id="head_buttons">\
<input type="button" class="button_style" id="btn_print" value="Imprimer">\
</div>\
<div id="navigation">\
<p>\
<a href="javascript:void(0);" class="link" id="lnk_close">Sortir</a>\
</p>\
</div>\
</div>',
    CONST_INDEX_COUNT:                '{1}-{2} of {0}',
    CONST_INDEX_EMPTY_COUNT:          '{0}',
    CONST_INDEX_PAGE_COUNT:           '[NUM]',
    CONST_INDEX_PAGER_FIRST:          'Primera',
    CONST_INDEX_PAGER_PREV:           'Ant.',
    CONST_INDEX_PAGER_NEXT:           'Sig.',
    CONST_INDEX_PAGER_LAST:           'Última',
    CONST_SEARCH_OPTION_REPAIR:       'Manual de reparaciones',
    CONST_SEARCH_OPTION_ALL:          'Todos',
    CONST_SEARCH_OPTION_LAYOUT:       'Ubicación de las piezas/Componentes',
    CONST_SEARCH_OPTION_TROUBLE:      'Solución de problemas (Cuadro de DTC/Síntomas del problema)',
    CONST_SEARCH_OPTION_REMOVE:       'Extracción/Instalación/Desmontaje/Sustitución/Ajuste',
    CONST_SEARCH_OPTION_MAINTENANCE:  'Inspeccion/Inspeccion en el vehículo',
    CONST_SEARCH_OPTION_CUSTOM:       'Parámetros personalizados',
    CONST_SEARCH_OPTION_PREPARED:     'Preparación',
    CONST_SEARCH_OPTION_OTHER:        'Otros',
    CONST_SEARCH_OPTION_NCF:          'Caract. nuevas vehíc.',
    CONST_SEARCH_OPTION_EWD:          'Diagrama del cableado electrico',
    CONST_SEARCH_OPTION_BRM:          'Reparación de la carrocería',
    CONST_SEARCH_OPTION_OM:           'Manual del propietario',
    CONST_SEARCH_OPTION_WEL:          'Manual del Welcab',
    CONST_SEARCH_OPTION_RES:          'Manual de rescate de híbridos',
    CONST_SEARCH_OPTION_ERG:          'Guía de respuesta a emergencias',
    CONST_SEARCH_OPTION_DM:           'Manual De Desmontaje Del Vehículo Híbrido',
    CONST_SEARCH_OPTION_RESET:        'Reinic.',
    CONST_SEARCH_OPTION_ENTRY:        'OK',
    CONST_SEARCH_OPTION_CLOSE:        'Cancelar',
    CONST_CAR_INFO_DESTINATION:       'Destino',
    CONST_CAR_INFO_BRAND:             'Marca',
    CONST_CAR_INFO_MODEL:             'Modelo',
    CONST_CAR_INFO_GENERALCODE:       'Código general',
    CONST_CAR_INFO_OPTION:            'Opción',
    CONST_CAR_INFO_PRODUCTIONDATE:    'Fecha fabricación',
    CONST_CAR_INFO_LANGUAGE:          'Idioma',
    CONST_TOP_BIND_MANUALS_DATE:      'Fecha de publicación: ',
    CONST_MANUAL_NAME_RM:             'Manual de reparaciones',
    CONST_MANUAL_NAME_NM:             'Caract. nuevas vehíc.',
    CONST_MANUAL_NAME_EM:             'Diagrama del cableado electrico',
    CONST_MANUAL_NAME_BM:             'Reparación de la carrocería',
    CONST_MANUAL_NAME_OM:             'Manual del propietario',
    CONST_MANUAL_NAME_WC:             'Manual del Welcab',
    CONST_MANUAL_NAME_HR:             'Manual de rescate de híbridos',
    CONST_MANUAL_NAME_ER:             'Guía de respuesta a emergencias',
    CONST_MANUAL_NAME_DM:             'Manual De Desmontaje Del Vehículo Híbrido',
    CONST_SERVICE_TAB_NAME_SR:        'Resultados',
    CONST_SERVICE_TAB_NAME_RM:        'Manual de reparaciones',
    CONST_SERVICE_TAB_NAME_NM:        'Caract. nuevas vehíc.',
    CONST_SERVICE_TAB_NAME_EM:        'Diagrama del cableado electrico',
    CONST_SERVICE_TAB_NAME_BM:        'Reparación de la carrocería',
    CONST_SERVICE_TAB_NAME_OM:        'Manual del propietario',
    CONST_SERVICE_TAB_NAME_WC:        'Manual del Welcab',
    CONST_SERVICE_TAB_NAME_HR:        'Manual de rescate de híbridos',
    CONST_SERVICE_TAB_NAME_ER:        'Guía de respuesta a emergencias',
    CONST_SERVICE_TAB_NAME_DM:        'Manual De Desmontaje Del Vehículo Híbrido',
    CONST_SERVICE_TAB_NAME_RF:        'Información de referencia',
    CONST_RESULT_KEYWORD:             'Resultados para "{keyword}"',
    CONST_RESULT_KEYWORD_YOKOGUSHI:   'Resultados YOKOGUSHI para "{keyword}"',
    CONST_RESULT_NUMBER:              '{min}-{max} of {all}',
    CONST_RESULT_RM:                  'Manual de reparaciones({result})',
    CONST_RESULT_NM:                  'Caract. nuevas vehíc.({result})',
    CONST_RESULT_EM:                  'Diagrama del cableado electrico({result})',
    CONST_RESULT_BM:                  'Reparación de la carrocería({result})',
    CONST_RESULT_OM:                  'Manual del propietario({result})',
    CONST_RESULT_WC:                  'Manual del Welcab({result})',
    CONST_RESULT_HR:                  'Manual de rescate de híbridos({result})',
    CONST_RESULT_ER:                  'Guía de respuesta a emergencias({result})',
    CONST_RESULT_DM:                  'Manual De Desmontaje Del Vehículo Híbrido({result})',
    CONST_RESULT_KEYWORD_LEN:         '28',
    CONST_CONTENTS_FIXED_HEAD:        '<div id="header_head"><div id="head_buttons"><input type="button" class="button_style" id="btn_print" value="印刷"></div><div id="navigation"><p><a href="javascript:void(0);" class="link" id="lnk_close">Salir</a></p></div></div>',
    CONST_CONTENTS_FIXED_FOOT:        '&copy; 2012 TOYOTA MOTOR CORPORATION. All Rights Reserved.',
    CONST_README_TTL:                 'Léame antes de empezar',
    CONST_DEFKEYROWD:                 'Palabra ',
    CONST_KEYWORD_NM:                 'Palabra ',
    CONST_LANGLIST_U:                 'North America',
    CONST_LANGLIST_J:                 'Japan',
    CONST_LANGLIST_E:                 'Europe and General'
};

/**
 * 初期化
 */
Dict.$init = function() {
  var METHODNAME = 'Dict.$init';
  try {

    var len = DictConst.Keys.length;
    //DictConstの定義を言語ごとの定義にセット
    for(var i = 0; i < len; i++) {
      var key = DictConst.Keys[i];
      Dict[key] = DictConst[key];
    }

  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
};
